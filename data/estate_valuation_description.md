#### A source ####
https://archive.ics.uci.edu/ml/datasets/Real+estate+valuation+data+set#

#### Brief description ####

The market historical data set of real estate valuation are collected from Sindian Dist., New Taipei City, Taiwan. The â€œreal estate valuationâ€ is a regression problem. The market historical data set of real estate valuation are collected from Sindian Dist., New Taipei City, Taiwan.

#### Relevant Papers ####

Yeh, I. C., & Hsu, T. K. (2018). Building real estate valuation models with comparative approach through case-based reasoning. Applied Soft Computing, 65, 260-271.

#### The target attribute ####

- Y=house price of unit area (10000 New Taiwan Dollar/Ping, where Ping is a local unit, 1 Ping = 3.3 meter squared) Real

#### Attribute Information ####

- X1=the transaction date (for example, 2013.250=2013 March, 2013.500=2013 June, etc.) Date

- X2=the house age (unit: year) Real

- X3=the distance to the nearest MRT station (unit: meter) Real

- X4=the number of convenience stores in the living circle on foot. Integer

- X5=the geographic coordinate, latitude. (unit: degree) Real

- X6=the geographic coordinate, longitude. (unit: degree) Real

